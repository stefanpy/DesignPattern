package javaInDemo;

/**
 * Create by stefan
 * Date on 2018-06-07  23:06
 * Convertion over Configuration!
 */
public class ThreadTest {
    public static void main(String[] args) {
        Lisi lisi = new Lisi();
        Hanfeizi hanfeizi = new Hanfeizi();
        hanfeizi.addObserver(lisi);

        Hanguojun hanguojun = new Hanguojun();
        hanguojun.addObserver(lisi);

        new Thread(){
            @Override
            public void run() {
                hanfeizi.haveBreakfast();
            }
        }.start();

        new Thread(){
            @Override
            public void run() {
                hanguojun.sleep();
            }
        }.start();
    }
}
