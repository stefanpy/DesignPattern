package javaInDemo;

import java.util.Observable;
import java.util.Observer;

/**
 * Create by stefan
 * Date on 2018-06-07  21:28
 * Convertion over Configuration!
 */
public class Lisi implements Observer {
    @Override
    public void update(Observable o, Object arg) {
        System.out.println(o);
        this.reportToQinShiHuang(o,arg);
    }

    //汇报给秦始皇
    public void reportToQinShiHuang(Observable o,Object arg){
        System.out.println("李斯报告----"+arg.toString());
    }
}
