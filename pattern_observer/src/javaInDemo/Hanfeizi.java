package javaInDemo;

import java.util.Observable;

/**
 * Create by stefan
 * Date on 2018-06-07  21:27
 * Convertion over Configuration!
 */
public class Hanfeizi extends Observable {
    public void haveBreakfast(){
        super.setChanged();
        super.notifyObservers("韩非子在吃饭");
    }
}
