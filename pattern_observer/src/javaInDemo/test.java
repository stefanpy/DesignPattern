package javaInDemo;


/**
 * Create by stefan
 * Date on 2018-06-07  21:30
 * Convertion over Configuration!
 */
public class test {
    public static void main(String[] args) {
        Lisi lisi = new Lisi();
        Hanfeizi hanfeizi = new Hanfeizi();
        hanfeizi.addObserver(lisi);
        hanfeizi.haveBreakfast();

        Hanguojun hanguojun = new Hanguojun();
        hanguojun.addObserver(lisi);
        hanguojun.sleep();


    }
}
