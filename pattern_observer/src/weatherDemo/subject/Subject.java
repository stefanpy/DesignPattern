package weatherDemo.subject;

import weatherDemo.observer.Observer;

/**
 * Create by stefan
 * Date on 2018-06-07  18:54
 * Convertion over Configuration!
 */
public interface Subject {
    public void registerObserver(Observer o);
    public void removeObserver(Observer o);
    public void notifyObservers();
}
