package weatherDemo.subject;

import weatherDemo.observer.Observer;

import java.util.ArrayList;
import java.util.List;

/**
 * Create by stefan
 * Date on 2018-06-07  19:01
 * Convertion over Configuration!
 */
public class WeatherDataSt implements Subject {
    private float temperatrue;
    private float pressure;
    private float humidity;

    private List<Observer> observers;
    public WeatherDataSt()
    {
        observers=new ArrayList<Observer>();
    }


    @Override
    public void registerObserver(Observer o) {
        observers.add(o);

    }

    @Override
    public void removeObserver(Observer o) {
        if (observers.contains(o)){
            observers.remove(o);
        }
    }

    @Override
    public void notifyObservers() {
        observers.forEach(observer -> observer.update(temperatrue,pressure,humidity));
    }

    public void setData(float temperatrue,float pressure,float humidity){
        this.temperatrue = temperatrue;
        this.pressure = pressure;
        this.humidity = humidity;
    }
}
