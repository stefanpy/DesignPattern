package weatherDemo.test;

import weatherDemo.observer.CurrentConditions;
import weatherDemo.observer.ForcastConditions;
import weatherDemo.observer.Observer;
import weatherDemo.subject.Subject;
import weatherDemo.subject.WeatherDataSt;

/**
 * Create by stefan
 * Date on 2018-06-07  19:17
 * Convertion over Configuration!
 */
public class WeatherTest {
    public static void main(String[] args) {
        Observer o1 = new CurrentConditions();
        Observer o2 = new ForcastConditions();

        WeatherDataSt subject = new WeatherDataSt();
        subject.registerObserver(o1);
        subject.registerObserver(o2);

        subject.setData(20,30,40);
        subject.notifyObservers();
    }
}
