package weatherDemo.observer;

/**
 * Create by stefan
 * Date on 2018-06-07  18:58
 * Convertion over Configuration!
 */
public class CurrentConditions implements Observer {
    private float teperatrue,pressure,humidity;
    @Override
    public void update(float teperatrue, float pressure, float humidity) {
        this.humidity = humidity;
        this.pressure = pressure;
        this.teperatrue = teperatrue;
        display();
    }

    public void display() {
        System.out.println("***Today teperatrue:" + teperatrue + "***");
        System.out.println("***Today pressure:" + pressure + "***");
        System.out.println("***Today humidity:" + humidity + "***");

    }
}
