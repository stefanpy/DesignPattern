package weatherDemo.observer;

/**
 * Create by stefan
 * Date on 2018-06-07  19:00
 * Convertion over Configuration!
 */
public class ForcastConditions implements Observer{
    private float temperatrue;
    private float pressure;
    private float humidity;
    @Override
    public void update(float temperatrue, float pressure, float humidity) {
        // TODO Auto-generated method stub
        this.temperatrue=temperatrue;
        this.pressure=pressure;
        this.humidity=humidity;

        display();
    }
    public void display()
    {
        System.out.println("**明天温度:"+(temperatrue+Math.random())+"**");
        System.out.println("**明天气压:"+(pressure+10*Math.random())+"**");
        System.out.println("**明天湿度:"+(humidity+Math.random())+"**");
    }
}
