package weatherDemo.observer;

/**
 * Create by stefan
 * Date on 2018-06-07  18:55
 * Convertion over Configuration!
 */
public interface Observer {
    public void update(float mTemperatrue,float mPressure,float mHumidity);
}
