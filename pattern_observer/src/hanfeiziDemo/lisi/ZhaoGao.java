package hanfeiziDemo.lisi;

/**
 * Create by stefan
 * Date on 2018-06-07  19:58
 * Convertion over Configuration!
 */
public class ZhaoGao implements Observer {
    @Override
    public void update(String context) {
        System.out.println("赵高：观察到韩国活动，开始汇报秦始皇。。。。");
        this.reportToQinShiHuang(context);
        System.out.println("赵高报告完毕");
    }

    //汇报给秦始皇
    public void reportToQinShiHuang(String context){
        System.out.println("赵高报告----"+context);
    }
}
