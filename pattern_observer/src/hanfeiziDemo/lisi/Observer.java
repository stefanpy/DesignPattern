package hanfeiziDemo.lisi;

/**
 * Create by stefan
 * Date on 2018-06-07  19:44
 * Convertion over Configuration!
 */
public interface Observer {
    void update(String context);
}
