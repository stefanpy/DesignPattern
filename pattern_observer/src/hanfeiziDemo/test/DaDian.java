package hanfeiziDemo.test;

import hanfeiziDemo.hanfeizi.HanFeiZi;
import hanfeiziDemo.hanfeizi.HanGuoJun;
import hanfeiziDemo.lisi.Lisi;
import hanfeiziDemo.lisi.Observer;
import hanfeiziDemo.lisi.ZhaoGao;

/**
 * Create by stefan
 * Date on 2018-06-07  19:59
 * Convertion over Configuration!
 */
public class DaDian {
    public static void main(String[] args) {
        Observer lisi = new Lisi();
        Observer zhaogao = new ZhaoGao();

        System.out.println("--------------监视韩非子---------------");
        HanFeiZi hanFeiZi = new HanFeiZi();

        hanFeiZi.addObserver(lisi);
        hanFeiZi.addObserver(zhaogao);

        hanFeiZi.haveBreakfast();
        hanFeiZi.haveFun();

        System.out.println("--------------监视韩国君---------------");

        HanGuoJun hanGuoJun = new HanGuoJun();
        hanGuoJun.addObserver(zhaogao);
        hanGuoJun.sleep();
    }
}
