package hanfeiziDemo.hanfeizi;


import hanfeiziDemo.lisi.Observer;

import java.util.ArrayList;
import java.util.List;

/**
 * Create by stefan
 * Date on 2018-06-07  19:46
 * Convertion over Configuration!
 */
public class HanFeiZi extends Observable {
    //韩非子要吃饭了
    public void haveBreakfast(){
        super.notifyObservers("韩非子在吃饭");
    }

    //韩非子在娱乐
    public void haveFun(){
        super.notifyObservers("韩非子在娱乐");
    }

}
