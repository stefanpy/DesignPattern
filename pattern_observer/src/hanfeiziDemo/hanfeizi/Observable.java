package hanfeiziDemo.hanfeizi;


import hanfeiziDemo.lisi.Observer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

/**
 * Create by stefan
 * Date on 2018-06-07  19:43
 * Convertion over Configuration!
 */
public abstract class  Observable {
    private List<Observer> observers = Collections.synchronizedList(new ArrayList<>());
    public void addObserver(Observer observer){
        this.observers.add(observer);
    }
    public void deleteObserver(Observer observer){
        this.observers.remove(observer);
    }
    public void notifyObservers(String context){
        observers.forEach(observer -> observer.update(context));
    }
}
