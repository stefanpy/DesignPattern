package duckDemo.duck;

import duckDemo.flyBehavior.FlyBehavior;
import duckDemo.quackBehavior.QuackBehavior;

/**
 * Create by stefan
 * Date on 2018-06-05  14:21
 * Convertion over Configuration!
 */
public abstract class Duck {
    FlyBehavior flyBehavior;
    QuackBehavior quackBehavior;
    public Duck(){

    }

    public void fly(){
        if (flyBehavior!=null){
            flyBehavior.fly();
        }
    }

    public void quack(){
        if (quackBehavior!=null){
            quackBehavior.quack();
        }
    }
    public abstract void eat();

    public void swim(){
        System.out.println("小鸭子swim--");
    }
}
