package duckDemo.duck;

import duckDemo.flyBehavior.GoodFlyBehavior;
import duckDemo.quackBehavior.GuGuQuackBehavior;

/**
 * Create by stefan
 * Date on 2018-06-05  15:03
 * Convertion over Configuration!
 */
public class BlueHeadDuck extends Duck {
    public BlueHeadDuck(){
        quackBehavior = new GuGuQuackBehavior();
    }
    @Override
    public void eat() {
        System.out.println("坐着吃");
    }
}
