package duckDemo.duck;

import duckDemo.flyBehavior.BadFlyBehavior;
import duckDemo.quackBehavior.GuGuQuackBehavior;

/**
 * Create by stefan
 * Date on 2018-06-05  14:54
 * Convertion over Configuration!
 */
public class RedHeadDuck extends Duck {

    public RedHeadDuck(){
        flyBehavior = BadFlyBehavior.getInstantiation();
        quackBehavior = new GuGuQuackBehavior();
    }
    @Override
    public void eat() {
        System.out.println("吃鱼---");
    }
}
