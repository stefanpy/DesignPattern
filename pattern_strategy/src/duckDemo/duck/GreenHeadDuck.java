package duckDemo.duck;

import duckDemo.flyBehavior.GoodFlyBehavior;
import duckDemo.quackBehavior.GaGaQuackBehavior;

/**
 * Create by stefan
 * Date on 2018-06-05  14:49
 * Convertion over Configuration!
 */
public class GreenHeadDuck extends Duck {

    public GreenHeadDuck(){
        flyBehavior = GoodFlyBehavior.getInstantiation();
        quackBehavior = new GaGaQuackBehavior();
    }
    @Override
    public void eat() {
        System.out.println("吃鱼");
    }


}
