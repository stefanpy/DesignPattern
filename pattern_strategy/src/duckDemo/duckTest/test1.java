package duckDemo.duckTest;

import duckDemo.duck.BlueHeadDuck;
import duckDemo.duck.Duck;
import duckDemo.duck.GreenHeadDuck;
import duckDemo.duck.RedHeadDuck;

/**
 * Create by stefan
 * Date on 2018-06-05  15:16
 * Convertion over Configuration!
 *
 * 策略模式：
 * 抽象类，分析不变与变
 * 变的分出接口
 */
public class test1 {
    public static void main(String[] args) {
        System.out.println("--------------绿头鸭-------------");
        Duck green = new GreenHeadDuck();
        green.fly();
        green.quack();
        System.out.println("--------------红头鸭-------------");
        Duck red = new RedHeadDuck();
        red.fly();
        red.quack();
        System.out.println("--------------蓝头鸭-------------");
        Duck blue = new BlueHeadDuck();
        blue.fly();
        blue.quack();
    }
}
