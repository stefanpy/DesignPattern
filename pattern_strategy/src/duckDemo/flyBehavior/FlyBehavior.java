package duckDemo.flyBehavior;

/**
 * Create by stefan
 * Date on 2018-06-05  14:42
 * Convertion over Configuration!
 */
public interface FlyBehavior {
    void fly();
}
