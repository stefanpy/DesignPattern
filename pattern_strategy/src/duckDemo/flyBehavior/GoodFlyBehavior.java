package duckDemo.flyBehavior;

/**
 * Create by stefan
 * Date on 2018-06-05  14:42
 * Convertion over Configuration!
 */
public class GoodFlyBehavior implements FlyBehavior {
    private GoodFlyBehavior(){

    }
    public  static  GoodFlyBehavior getInstantiation(){
        return new GoodFlyBehavior();
    }
    @Override
    public void fly() {
        System.out.println("good-----fly");
    }
}
