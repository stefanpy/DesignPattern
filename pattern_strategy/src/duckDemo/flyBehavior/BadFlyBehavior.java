package duckDemo.flyBehavior;

/**
 * Create by stefan
 * Date on 2018-06-05  14:55
 * Convertion over Configuration!
 */
public class BadFlyBehavior implements FlyBehavior{
    private BadFlyBehavior(){

    }
    public static BadFlyBehavior getInstantiation(){
        return new BadFlyBehavior();
    }
    @Override
    public void fly() {
        System.out.println("bad----fly");
    }
}
