package duckDemo.quackBehavior;

/**
 * Create by stefan
 * Date on 2018-06-05  14:44
 * Convertion over Configuration!
 */
public interface QuackBehavior {
    void quack();
}
