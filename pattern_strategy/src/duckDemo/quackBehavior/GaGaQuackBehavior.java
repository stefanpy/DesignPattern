package duckDemo.quackBehavior;

/**
 * Create by stefan
 * Date on 2018-06-05  14:45
 * Convertion over Configuration!
 */
public class GaGaQuackBehavior implements QuackBehavior {
    @Override
    public void quack() {
        System.out.println("gaga---quack");
    }
}
