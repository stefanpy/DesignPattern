package duckDemo.quackBehavior;

/**
 * Create by stefan
 * Date on 2018-06-05  14:56
 * Convertion over Configuration!
 */
public class GuGuQuackBehavior implements QuackBehavior {
    @Override
    public void quack() {
        System.out.println("gugu---quack");
    }
}
