package sanguoDemo;

/**
 * Create by stefan
 * Date on 2018-06-05  17:52
 * Convertion over Configuration!
 */
public class ZhaoYun {
    public static void main(String[] args) {
        Context context = null;
        System.out.println("---刚刚到吴国的时候拆第一个锦囊");
        context = new Context(new BackDoor());
        context.operate();

        System.out.println("---刘备乐不思蜀，拆开第二个");

        context = new Context(new GivenGreenLight());
        context.operate();

        System.out.println("---孙权的小兵追了，拆第三个");
        context  = new Context(new BlockEnemy());
        context.operate();
    }
}
