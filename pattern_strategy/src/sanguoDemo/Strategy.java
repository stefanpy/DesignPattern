package sanguoDemo;

/**
 * Create by stefan
 * Date on 2018-06-05  17:47
 * Convertion over Configuration!
 */
public interface Strategy {
    void operate();
}
