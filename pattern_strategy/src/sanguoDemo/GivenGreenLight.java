package sanguoDemo;

/**
 * Create by stefan
 * Date on 2018-06-05  17:49
 * Convertion over Configuration!
 */
public class GivenGreenLight implements Strategy {
    @Override
    public void operate() {
        System.out.println("求吴国太开绿灯，放行");
    }
}
