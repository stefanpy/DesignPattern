package sanguoDemo;

/**
 * Create by stefan
 * Date on 2018-06-05  17:51
 * Convertion over Configuration!
 * 装锦囊的袋子
 */
public class Context {
    private Strategy strategy;
    public Context(Strategy strategy){
        this.strategy = strategy;
    }

    //使用计谋
    public void operate(){
        this.strategy.operate();
    }
}
