package calculatorDemo.ordinaryStrategy;

/**
 * Create by stefan
 * Date on 2018-06-05  17:15
 * Convertion over Configuration!
 */
public class Add implements Calculator {
    @Override
    public int exec(int a, int b) {
        return a+b;
    }
}
