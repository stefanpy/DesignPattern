package calculatorDemo.ordinaryStrategy;

/**
 * Create by stefan
 * Date on 2018-06-05  17:16
 * Convertion over Configuration!
 */
public class Sub implements Calculator {
    @Override
    public int exec(int a, int b) {
        return a-b;
    }
}
