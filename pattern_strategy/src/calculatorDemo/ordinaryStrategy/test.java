package calculatorDemo.ordinaryStrategy;

/**
 * Create by stefan
 * Date on 2018-06-05  17:20
 * Convertion over Configuration!
 */
public class test {
    public static void main(String[] args) {
        Context context = new Context(new Add());
        int result = context.exec(10,10);
        System.out.println(result);

        Context context1 = new Context(new Sub());
        int result1 = context1.exec(20,10);
        System.out.println(result1);
    }
}
