package calculatorDemo.ordinaryStrategy;

/**
 * Create by stefan
 * Date on 2018-06-05  17:17
 * Convertion over Configuration!
 */
public class Context {
    private Calculator cal = null;
    public Context(Calculator cal){
        this.cal = cal;
    }

    public int exec(int a,int b){
        return  this.cal.exec(a,b);
    }

}
