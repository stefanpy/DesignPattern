package calculatorDemo.ordinaryStrategy;

/**
 * Create by stefan
 * Date on 2018-06-05  17:14
 * Convertion over Configuration!
 */
public interface Calculator {
    int exec(int a,int b);
}
