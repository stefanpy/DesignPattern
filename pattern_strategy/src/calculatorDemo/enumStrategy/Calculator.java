package calculatorDemo.enumStrategy;

/**
 * Create by stefan
 * Date on 2018-06-05  16:49
 * Convertion over Configuration!
 * 策略枚举，是一个非常优秀和方便的模式，但是它受枚举类型的限制，
 * 每个枚举项都是public、final、static的，扩展性受到了一定的约束，
 * 所以在系统开发中，策略枚举一般担当不经常发生变化的角色
 *
 */
public enum Calculator {
    //+
    ADD ("+"){
        public int exec(int a,int b){
            return a+b;
        }
    },
    SUB("-"){
        public int exec(int a,int b){
            return a-b;
        }
    };
    String value = "";

    //定义成员值类型
    private Calculator(String _value){
        this.value = _value;
    }

    //获得枚举成员的值
    public String getValue(){
        return this.value;
    }

    //声明一个抽象函数
    public abstract int exec(int a,int b);
}
