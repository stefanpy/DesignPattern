package calculatorDemo.enumStrategy;

/**
 * Create by stefan
 * Date on 2018-06-05  16:54
 * Convertion over Configuration!
 */
public class Client {
    public static void main(String[] args) {
        int add_result = Calculator.ADD.exec(10,20);
        int sub_result = Calculator.SUB.exec(20,10);

        System.out.println(add_result);
        System.out.println(sub_result);
    }
}
